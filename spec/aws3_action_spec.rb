describe Fastlane::Actions::Aws3Action do
  describe '#run' do
    it 'prints a message' do
      expect(Fastlane::UI).to receive(:message).with("The aws3 plugin is working!")

      Fastlane::Actions::Aws3Action.run(nil)
    end
  end
end
