require 'aws-sdk-s3'
require 'net/http'
require 'fastlane/action'
require_relative '../helper/aws3_helper'

module Fastlane
  module Actions
    module SharedValues
      AWS_S3_URL = :AWS_S3_URL
    end
    class Aws3GetFileAction < Action
      def self.run(params)

        Actions.verify_gem!('aws-sdk-s3')

        bucket_name = params[:bucket]

        file_name = params[:content_path]
        UI.message "file_name: #{file_name}"

        s3 = Aws::S3::Resource.new(
            credentials: Aws::Credentials.new(params[:access_key_id], params[:secret_access_key]),
            region: 'eu-west-1'
        )

        bucket = s3.buckets.find(bucket_name)
        if bucket.nil?
          UI.error "Bucket '#{bucket_name}' not found, please verify bucket and credentials 🚫"
        end

        obj = s3.bucket(bucket_name).objects(file_name)
        if obj.nil?
          UI.error "Object '#{file_name}' not found, please verify file name 🚫"
          return false
        end

        destination = params[:destination]
        UI.message "Destination: #{destination}"
        begin
          s3.bucket(bucket_name).object(file_name).download_file(destination)
        rescue => e
          UI.error "Throw: #{e}"
          UI.error "File '#{file_name}' not found, please verify file name 🚫"
          return false
        end

        return true
      end

      def self.description
        "Upload custom file to s3 and get the signed url"
      end

      def self.authors
        ["Alexander"]
      end

      def self.return_value
        # If your method provides a return value, you can describe here what it does
      end

      def self.details
        # Optional:
        "Support AWS S3"
      end

      def self.available_options
        [
            FastlaneCore::ConfigItem.new(key: :access_key_id,
                                         env_name: "S3_ACTIONS_ACCESS_KEY_ID",
                                         description: "AWS Access Key",
                                         optional: false,
                                         type: String),
            FastlaneCore::ConfigItem.new(key: :secret_access_key,
                                         env_name: "S3_ACTIONS_SECRET_ACCESS_KEY",
                                         description: "AWS Secret Access Key",
                                         optional: false,
                                         type: String),
            FastlaneCore::ConfigItem.new(key: :bucket,
                                         env_name: "S3_ACTIONS_BUCKET",
                                         description: "S3 Bucket",
                                         optional: false,
                                         type: String),
            FastlaneCore::ConfigItem.new(key: :content_path,
                                         env_name: "S3_ACTIONS_UPLOAD_CONTENT_PATH",
                                         description: "Path for file to download",
                                         optional: false,
                                         type: String,
                                         verify_block: proc do |value|
                                           if value.nil? || value.empty?
                                             UI.user_error!("No content path for S3_sownload action given, pass using `content_path: 'path/to/file.txt'`")
                                           end
                                         end),
            FastlaneCore::ConfigItem.new(key: :destination,
                                         env_name: "S3_ACTIONS_FILE_NAME",
                                         description: "File name",
                                         optional: true,
                                         type: String)
        ]
      end

      def self.output
        [
            ['AWS_S3_URL', 'The url of the uploaded file']
        ]
      end

      def self.is_supported?(platform)
        # Adjust this if your plugin only works for a particular platform (iOS vs. Android, for example)
        # See: https://github.com/fastlane/fastlane/blob/master/fastlane/docs/Platforms.md
        #
        # [:ios, :mac, :android].include?(platform)
        true
      end
    end
  end
end
