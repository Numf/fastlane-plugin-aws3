module Fastlane
  module Helper
    class Aws3Helper
      # class methods that you define here become available in your action
      # as `Helper::Aws3Helper.your_method`
      #
      def self.show_message
        UI.message("Hello from the aws3 plugin helper!")
      end
    end
  end
end
